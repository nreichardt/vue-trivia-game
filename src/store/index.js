import Vue from "vue";
import Vuex from "vuex";
import { GameAPI } from "@/components/Util/GameAPI";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    gameDifficulty: "",
    gameAmountOfQuestions: "5",
    gameCategoryId: "",
    gameCategories: [],
    gameSetupFinished: false,

    gameIsActive: true,
    currentGameQuestion: 0,
    loadingQuestions: true,
    gameQuestions: [],
    userAnswers: [],
    error: "",
  },
  mutations: {
    toggleGameSetupFinished: (state) => {
      state.gameSetupFinished = !state.gameSetupFinished;
    },
    toggleGameIsActive: (state) => {
      state.gameIsActive = !state.gameIsActive;
    },
    setNextGameQuestion: (state) => {
      state.currentGameQuestion++;
    },
    setGameDifficulty: (state, payload) => {
      state.gameDifficulty = payload;
    },
    setGameAmountOfQuestions: (state, payload) => {
      state.gameAmountOfQuestions = payload;
    },
    setGameCategoryId: (state, payload) => {
      state.gameCategoryId = payload;
    },
    setGameCategories: (state, payload) => {
      state.gameCategories = payload;
    },
    setGameQuestions: (state, payload) => {
      state.gameQuestions = payload;
    },
    setCurrentGameQuestion: (state, payload) => {
      state.currentGameQuestion = payload;
    },
    addUserAnswer: (state, payload) => {
      state.userAnswers.push(payload);
    },
    resetUserAnswer: (state) => {
      state.userAnswers = new Array();
    },
    setError: (state, payload) => {
      state.error = payload;
    },
  },
  getters: {
    getGameCategories: (state) => {
      return state.gameCategories;
    },
    getGameQuestions: (state) => {
      return state.gameQuestions;
    },
    getQuestionAnswers: (state, index) => {
      return state.gameQuestions[index].answers;
    },
    getGameSetupFinished: (state) => {
      return state.gameSetupFinished;
    },
    getGameIsActive: (state) => {
      return state.gameIsActive;
    },
    getCurrentGameQuestion: (state) => {
      return state.currentGameQuestion;
    },
    getLoadingQuestions: (state) => {
      return state.loadingQuestions;
    },
    getUserAnswers: (state) => {
      return state.userAnswers;
    },
    getTotalScore: (state) => {
      let score = 0;
      for (let i = 0; i < state.gameQuestions.length; i++) {
        if (state.gameQuestions[i].correct_answer == state.userAnswers[i]) {
          score += 10;
        }
      }
      return score;
    },
  },
  actions: {
    async fetchCategories({ commit }) {
      try {
        const categories = await GameAPI.fetchCategories();
        commit("setGameCategories", categories);
      } catch (e) {
        commit("setError", e.message);
      }
    },
    async fetchGameQuestions({ commit, state }) {
      try {
        let questions = await GameAPI.fetchGameQuestions(
          state.gameAmountOfQuestions,
          state.gameCategoryId,
          state.gameDifficulty
        );
        questions.forEach((element) => {
          element.answers.sort(() => Math.random() - 0.5);
        });
        commit("setGameQuestions", questions);
        state.loadingQuestions = false;
      } catch (e) {
        commit("setError", e.message);
      }
    },
  },
});
