export const GameAPI = {
  fetchCategories() {
    return fetch("https://opentdb.com/api_category.php")
      .then((response) => response.json())
      .then((data) => data.trivia_categories);
  },
  fetchGameQuestions(questionsAmount, categoryId, difficulty) {
    return fetch(
      `https://opentdb.com/api.php?amount=${questionsAmount}&category=${categoryId}&difficulty=${difficulty}`
    )
      .then((response) => response.json())
      .then((data) =>
        data.results.map((question) => {
          question.answers = [
            question.correct_answer,
            ...question.incorrect_answers,
          ];
          return question;
        })
      );
  },
};
