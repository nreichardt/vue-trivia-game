import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "GameSetup",
    component: () =>
      import(
        /* webpackChunkName: "gameSetup" */ "../components/GameSetup/GameSetup.vue"
      ),
  },
  {
    path: "/game",
    name: "Game",
    component: () =>
      import(/* webpackChunkName: "game" */ "../components/Game/Game.vue"),
  },
  {
    path: "/gameResult",
    name: "GameResult",
    component: () =>
      import(
        /* webpackChunkName: "gameResult" */ "../components/GameResult/GameResult.vue"
      ),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
